const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
const spanGreetings = document.querySelector('.span-greetings');

// Alternatives for documents.querySelector()
/*
    document.getElementById('txt-firstName');
    document.getElementByClassName('txt-inputs');
    document.getElementByTagName('input);
*/

// txtLastName.addEventListener('keyup', (event) => {
//     spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;
//     console.log(event.target.value);
// })


function updateFullName() {
    const fullName = `${txtFirstName.value} ${txtLastName.value}`;
    spanFullName.innerHTML = fullName.trim();
  }
  
  txtFirstName.addEventListener("keyup", (event) => {
    console.log(event.target);
    console.log(event.target.value);
    updateFullName();
  });
  
  txtLastName.addEventListener("keyup", (event) => {
    console.log(event.target);
    console.log(event.target.value);
    updateFullName();
  });

// txtFirstName.addEventListener('keyup', (event) => {
//     console.log(event.target);
//     console.log(event.target.value);
//     console.log(greetings);
// })

// Additional Example & Mini Activity
// const greetings = () => {
//     let firstName = txtFirstName.value;
//     let greeting = 'Hi, Welcome!';
//     spanGreetings.innerHTML = 'Hi, Welcome! ' + firstName;
// }



